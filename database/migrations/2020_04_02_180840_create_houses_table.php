<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */

    public function up()
    {
        Schema::create('houses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('description')->nullable();
            $table->float('longitude',10,6)->nullable();
            $table->float('latitude',10,6)->nullable();
            $table->integer('bedrooms')->nullable();
            $table->integer('size')->nullable();
            $table->integer('floor')->nullable();
            $table->integer('heat')->nullable();
            $table->integer('year')->nullable();
            $table->boolean('newBuild')->nullable();
            $table->double('price',16,3)->nullable();
            $table->boolean('singleFloor')->nullable();
            $table->boolean('hasElevator')->nullable();
            $table->timestamps();
            $table->unsignedBigInteger('supermarket_id')->nullable();
            $table->unsignedBigInteger('bus_id')->nullable();
            $table->unsignedBigInteger('citycenter_id')->nullable();
            $table->unsignedBigInteger('gym_id')->nullable();
            $table->unsignedBigInteger('metro_id')->nullable();
            $table->unsignedBigInteger('park_id')->nullable();
            $table->unsignedBigInteger('pharmacy_id')->nullable();
            $table->unsignedBigInteger('school_id')->nullable();

            $table->integer('busD')->nullable();
            //$table->boolean('busD2')->default(false)->nullable();
            //$table->boolean('busD3')->default(false)->nullable();
            $table->integer('cityCenterD')->nullable();
           // $table->boolean('cityCenterD2')->default(false)->nullable();
            //$table->boolean('cityCenterD3')->default(false)->nullable();
            $table->integer('gymD')->nullable();
            //$table->boolean('gymD2')->default(false)->nullable();
            //$table->boolean('gymD3')->default(false)->nullable();
            $table->integer('metroD')->nullable();
            //$table->boolean('metroD2')->default(false)->nullable();
            //$table->boolean('metroD3')->default(false)->nullable();
            $table->integer('parkD')->nullable();
            //$table->boolean('parkD2')->default(false)->nullable();
            //$table->boolean('parkD3')->default(false)->nullable();
            $table->integer('pharmacyD')->nullable();
            //$table->boolean('pharmacyD2')->default(false)->nullable();
            //$table->boolean('pharmacyD3')->default(false)->nullable();
            $table->integer('schoolD')->nullable();
            //$table->boolean('schoolD2')->default(false)->nullable();
            //$table->boolean('schoolD3')->default(false)->nullable();
            $table->integer('supermarketD')->nullable();
            //$table->boolean('supermarketD2')->default(false)->nullable();
            //$table->boolean('supermarketD3')->default(false)->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('houses');
    }
}
