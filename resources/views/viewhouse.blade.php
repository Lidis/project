@extends('layouts.app')


@section('content')
    <div class="container">
        <div class="row justify-content-center">
            @foreach($search as $house)
            <div class="col-md-4 text-truncate m-1 ">
                <div class="card m-1">
                    <div class="card-body bg-info text-dark">
                        <h5 class="card-body">
                            {{$house->floor}}
                        </h5>
                        <h6 class="card-subtitle">
                            {{$house->longitude}} {{$house->latitude}}
                        </h6>
                        <p class="card-text">
                            {{$house->description}}
                        </p>
                    </div>
                </div>
            </div>
                @endforeach
        </div>
    </div>
@endsection

