<?php

namespace App\Http\Controllers;

use App\Bus;
use App\CityCenter;
use App\Gym;
use App\House;
use App\Metro;
use App\Park;
use App\School;
use App\Supermarket;
use Illuminate\Http\Request;
use PhpParser\Node\Stmt\Foreach_;
use Illuminate\Support\Collection;
use App\Pharmacy;

class HouseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Respons
     */
    public function index(Request $request)
    {

        $results = House::All();
        return view('resultsview', compact('results'));

            /*if ($request->has("search")) {
                $results = $request->search;
                return view('viewhouse', compact('results'));
            } else {
                $results = House::All();
                return view('resultsview', compact('results'));
            }*/

    }

    public function view($request){
        return $request;
        return view('viewhouse', compact('search'));
    }

    public function initialize()
    {
        $houses = House::all();
        $pharmacies = \App\Pharmacy::all();
        $metros = Metro::all();
        $supermarkets = Supermarket::all();
        $schools = School::all();
        $gyms = Gym::all();
        $parks = Park::all();
        $cityCenters = CityCenter::all();
        $buses = Bus::all();


        //$table = new collection;

        foreach($houses as $house)
        {

           $a = $house->closest($pharmacies);
           $b = $house->closest($metros);
           $c = $house->closest($supermarkets);
           $d = $house->closest($schools);
           $e = $house->closest($gyms);
           $f = $house->closest($parks);
           $g = $house->closest($cityCenters);
           $h = $house->closest($buses);

            $house->pharmacyD = $a['dist'];
            $house->metroD = $b['dist'];
            $house->supermarketD= $c['dist'];
            $house->schoolD = $d['dist'];
            $house->gymD = $e['dist'];
            $house->parkD = $f['dist'];
            $house->cityCenterD = $g['dist'];
            $house->busD = $h['dist'];

          /*if ($a['dist']<=500)
           {
               $house->pharmacyD1 = true;
               $house->pharmacyD2 = true;
               $house->pharmacyD3 = true;
           }
           elseif ($a['dist']<=1000)
           {
               $house->pharmacyD2 = true;
               $house->pharmacyD3 = true;
           }
           elseif ($a['dist']<=3000)
           {
               $house->pharmacyD3 = true;
           }
           else
           {

           }

            if ($b['dist']<=500)
            {
                $house->metroD1 = true;
                $house->metroD2 = true;
                $house->metroD3 = true;
            }
            elseif ($b['dist']<=1000)
            {
                $house->metroD2 = true;
                $house->metroD3 = true;
            }
            elseif ($b['dist']<=3000)
            {
                $house->metroD3 = true;
            }
            else
            {

            }

            if ($c['dist']<=500)
            {
                $house->supermarketD1 = true;
                $house->supermarketD2 = true;
                $house->supermarketD3 = true;
            }
            elseif ($c['dist']<=1000)
            {
                $house->supermarketD2 = true;
                $house->supermarketD3 = true;
            }
            elseif ($c['dist']<=3000)
            {
                $house->supermarketD3 = true;
            }
            else
            {

            }

            if ($d['dist']<=500)
            {
                $house->schoolD1 = true;
                $house->schoolD2 = true;
                $house->schoolD3 = true;
            }
            elseif ($d['dist']<=1000)
            {
                $house->schoolD2 = true;
                $house->schoolD3 = true;
            }
            elseif ($d['dist']<=3000)
            {
                $house->schoolD3 = true;
            }
            else
            {

            }

            if ($e['dist']<=500)
            {
                $house->gymD1 = true;
                $house->gymD2 = true;
                $house->gymD3 = true;
            }
            elseif ($e['dist']<=1000)
            {
                $house->gymD2 = true;
                $house->gymD3 = true;
            }
            elseif ($e['dist']<=3000)
            {
                $house->gymD3 = true;
            }
            else
            {

            }

            if ($f['dist']<=500)
            {
                $house->parkD1 = true;
                $house->parkD2 = true;
                $house->parkD3 = true;
            }
            elseif ($f['dist']<=1000)
            {
                $house->parkD2 = true;
                $house->parkD3 = true;
            }
            elseif ($f['dist']<=3000)
            {
                $house->parkD3 = true;
            }
            else
            {

            }

            if ($g['dist']<=500)
            {
                $house->cityCenterD1 = true;
                $house->cityCenterD2 = true;
                $house->cityCenterD3 = true;
            }
            elseif ($g['dist']<=1000)
            {
                $house->cityCenterD2 = true;
                $house->cityCenterD3 = true;
            }
            elseif ($g['dist']<=3000)
            {
                $house->cityCenterD3 = true;
            }
            else
            {

            }

            if ($h['dist']<=500)
            {
                $house->busD1 = true;
                $house->busD2 = true;
                $house->busD3 = true;
            }
            elseif ($h['dist']<=1000)
            {
                $house->busD2 = true;
                $house->busD3 = true;
            }
            elseif ($h['dist']<=3000)
            {
                $house->busD3 = true;
            }
            else
            {

            }*/


           $house->save();
        }

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $house = new House;
        $pharmacies = \App\Pharmacy::all();
        $metros = Metro::all();
        $supermarkets = Supermarket::all();
        $schools = School::all();
        $gyms = Gym::all();
        $parks = Park::all();
        $cityCenters = CityCenter::all();
        $buses = Bus::all();


        $a = $house->closest($pharmacies);
        $b = $house->closest($metros);
        $c = $house->closest($supermarkets);
        $d = $house->closest($schools);
        $e = $house->closest($gyms);
        $f = $house->closest($parks);
        $g = $house->closest($cityCenters);
        $h = $house->closest($buses);

        $house->pharmacyD = $a['dist'];
        $house->metroD = $b['dist'];
        $house->supermarketD= $c['dist'];
        $house->schoolD = $d['dist'];
        $house->gymD = $e['dist'];
        $house->parkD = $f['dist'];
        $house->cityCenterD = $g['dist'];
        $house->busD = $h['dist'];

            /*if ($a['dist']<=500)
            {
                $house->pharmacyD1 = true;
            }
            elseif ($a['dist']<=1000)
            {
                $house->pharmacyD2 = true;
            }
            elseif ($a['dist']<=3000)
            {
                $house->pharmacyD3 = true;
            }
            else
            {

            }
            if ($b['dist']<=500)
            {
                $house->metroD1 = true;
            }
            elseif ($b['dist']<=1000)
            {
                $house->metroD2 = true;
            }
            elseif ($b['dist']<=3000)
            {
                $house->metroD3 = true;
            }
            else
            {

            }

            if ($c['dist']<=500)
            {
                $house->supermarketD1 = true;
            }
            elseif ($c['dist']<=1000)
            {
                $house->supermarketD2 = true;
            }
            elseif ($c['dist']<=3000)
            {
                $house->supermarketD3 = true;
            }
            else
            {

            }

            if ($d['dist']<=500)
            {
                $house->schoolD1 = true;
            }
            elseif ($d['dist']<=1000)
            {
                $house->schoolD2 = true;
            }
            elseif ($d['dist']<=3000)
            {
                $house->schoolD3 = true;
            }
            else
            {

            }

            if ($e['dist']<=500)
            {
                $house->gymD1 = true;
            }
            elseif ($e['dist']<=1000)
            {
                $house->gymD2 = true;
            }
            elseif ($e['dist']<=3000)
            {
                $house->gymD3 = true;
            }
            else
            {

            }

            if ($f['dist']<=500)
            {
                $house->parkD1 = true;
            }
            elseif ($f['dist']<=1000)
            {
                $house->parkD2 = true;
            }
            elseif ($f['dist']<=3000)
            {
                $house->parkD3 = true;
            }
            else
            {

            }

            if ($g['dist']<=500)
            {
                $house->cityCenterD1 = true;
            }
            elseif ($g['dist']<=1000)
            {
                $house->cityCenterD2 = true;
            }
            elseif ($g['dist']<=3000)
            {
                $house->cityCenterD3 = true;
            }
            else
            {

            }

            if ($h['dist']<=500)
            {
                $house->busD1 = true;
            }
            elseif ($h['dist']<=1000)
            {
                $house->busD2 = true;
            }
            elseif ($h['dist']<=3000)
            {
                $house->busD3 = true;
            }
            else
            {

            }*/

            $house->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\House  $house
     * @return \Illuminate\Http\Response
     */
    public function show(House $house)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\House  $house
     * @return \Illuminate\Http\Response
     */
    public function edit(House $house)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\House  $house
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, House $house)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\House  $house
     * @return \Illuminate\Http\Response
     */
    public function destroy(House $house)
    {
        //
    }
}
