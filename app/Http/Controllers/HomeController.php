<?php

namespace App\Http\Controllers;

use App\Models\House;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance
     *
     * @return void
     */
    /*public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
//        $houses = House::all();

        $houses = DB::select("select * from houses");

        return view('home')
            ->with("houses", $houses);
    }

    public function test () {

        //return "Bill eisai kai o prwtos";

        $houses= House::All();

        return view('resultView',compact('houses'));

    }


    public function miavlakeia ($miavlakeia) {

        return "Bill eisai kai o prwtos".$miavlakeia;
    }

}


