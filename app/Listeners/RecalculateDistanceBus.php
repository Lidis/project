<?php

namespace App\Listeners;

use App\Bus;
use App\Events\BusAdded;
use App\House;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class RecalculateDistanceBus
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BusAdded  $event
     * @return void
     */
    public function handle(BusAdded $event)
    {
        $houses=House::All();
        $buses=Bus::All();

        foreach ($houses as $house) {

            $h = $house->closest($buses);

            if ($h['dist']<=500)
            {
                $house->busD1 = true;
            }
            elseif ($h['dist']<=1000)
            {
                $house->busD2 = true;
            }
            elseif ($h['dist']<=3000)
            {
                $house->busD3 = true;
            }
            else
            {

            }
            $house->save();
        }

    }
}
